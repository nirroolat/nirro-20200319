#This was a helpful resource to showcase how to leverage a multistage build and do a proper build of the web app https://maksood.medium.com/deploy-a-react-app-on-kubernetes-using-docker-129a75b79d58

#first stage of the build
FROM node:13.12.0-alpine AS builder
ENV CI=true
WORKDIR /app
COPY ./package.json /app
RUN npm install
RUN npm install react-scripts@3.4.1 -g

COPY . /app

RUN npm run-script build
# Run tests
RUN npm test
CMD npm start
#second stage of the build - copy build directory to /usr/share/nginx/html to be served up by nginx

#FROM nginx:alpine
#COPY --from=builder /app/build /emoji-search
#RUN rm /etc/nginx/conf.d/default.conf
#COPY nginx/nginx.conf /etc/nginx/conf.d
#EXPOSE 80
#CMD ["nginx", "-g", "daemon off;"]
