Emoji Search
---

This project should be configured to perform a Docker build / test / push on commit by utilizing gitlab runners.

# Observations

This project isn't actively maintained and there are many deprecated packages that need to be updated by the maintainer. A vulnerability scan would pick up these issues and required remediation.

The pods are restricted from running as privileged (set via securityContext)

# K8S / Tilt 

## cd into the k8s directory and run 
```
tilt up 
```
##This is assuming that you have tilt installed on your local machine - Link to tilt installer - https://tilt.dev 

##The emoji search app can be accessed here:

```
https://emoji.nirro.net
```

#Ways to improve

This was my first time using gitlab in any capacity so there was a bit of a learning curve to overcome. Overall if I had a little more time to work on these projects, I would spend more time optimizing the build process and see if there are ways to speed it up. Also addressing the vulnerabilities by updating packages would be recommended. It would require some testing though to ensure newer packages don't break any features or functionality
